FROM openjdk:17-jdk-slim
COPY target/*.jar atd-app.jar
EXPOSE 8088
ENTRYPOINT ["java", "-jar", "/atd-app.jar"]