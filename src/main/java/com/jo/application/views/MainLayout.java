package com.jo.application.views;

import com.jo.application.components.appnav.AppNav;
import com.jo.application.components.appnav.AppNavItem;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.vaadin.lineawesome.LineAwesomeIcon;

/**
 * The main view is a top-level placeholder for other views.
 */
@PageTitle("Main")
@Route(value = "")
//@CssImport(value = "./styles/timeline-items-style.css")
public class MainLayout extends CoreMainLayout {
    public MainLayout() {
        super();
        Image logo = new Image("themes/ampere-v2/images/logo.jpg", "Logo");
        this.addToDrawer(logo);

        handleLoginUser();

        setNavigation(getNavigation(), "Artedomus");
    }

    @Override
    protected AppNav getNavigation() {
        nav = new AppNav();

        AppNavItem parent = null;

        parent = new AppNavItem("ATD Mobile App");
        parent.setIcon(LineAwesomeIcon.STREAM_SOLID.create());
        nav.addItem(parent);

        parent.addItem(new AppNavItem("Shipment", "shipment", LineAwesomeIcon.USER.create())
                .withParameter("layout", this.getClass().getName()));


        nav.setClassName("app-nav");
        return nav;
    }

    private void handleLoginUser()
    {
        if (VaadinSession.getCurrent().getAttribute("user") != null) {
            return;
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        String username = auth.getName();
        VaadinSession.getCurrent().setAttribute("user", username);


    }
}