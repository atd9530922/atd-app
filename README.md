# atd-app
## ATD Mobile App


## Notes to build and run in standalone
mvn clean package -Pproduction

cd target

java -jar atd-app-1.0-SNAPSHOT.jar

## Notes to build and run in docker
mvn clean install -Pproduction

docker build . -t atd-app:latest

docker run --net=host -env JAVA_OPTS=-Dspring.datasource.url=jdbc:postgresql://localhost:5432/atd-app?currentSchema=adempiere atd-app:latest

