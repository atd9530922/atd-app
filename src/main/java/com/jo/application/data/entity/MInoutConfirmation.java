package com.jo.application.data.entity;

import com.jo.application.annotations.ContentDisplayedInSelect;
import com.jo.application.annotations.DisplayName;
import com.jo.application.annotations.component.CustomComponent;
import com.jo.application.core.data.entity.ZJTEntity;
import com.vaadin.flow.router.PageTitle;
import jakarta.persistence.*;
import org.hibernate.annotations.Immutable;


/**
 * Entity implementation class for Entity: ZJTUser
 */
@Entity
@Immutable
@Table(name = "m_inout_confirmation")
@PageTitle("Shipment Confirmation")
public class MInoutConfirmation implements ZJTEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "m_inout_id")
    private int m_inout_id;




    @Column(name="documentno")
    @DisplayName(value = "Document No")
    private String documentno;

    @Column(name="description")
    @DisplayName(value = "Description")
    private String description;

    @Column(name="customername")
    @DisplayName(value = "Customer")
    private String customername;

    @Column(name="address")
    @DisplayName(value = "Address")
    private String address;

    @Column(name="receivedby")
    @DisplayName(value = "Received By")
    private String receivedby;

    @Column(name="confirmed")
    @DisplayName(value ="Ship Confirmed")
    private boolean confirmed;

    @Column(name="bt_signature_id")
    @DisplayName(value = "Signature")
    @CustomComponent(value = "Signature")
    private int bt_signature_id;
    @Column(name = "ad_camera_id")
    @DisplayName(value = "Camera")
    @CustomComponent(value = "Camera")
    private int[] adCameraId;


    @Override
    public int getId() {
        return m_inout_id;
    }


    public int getBt_signature_id() {
        return bt_signature_id;
    }

    public void setBt_signature_id(int bt_signature_id) {
        this.bt_signature_id = bt_signature_id;
    }

    public String getDocumentno() {
        return documentno;
    }

    public void setDocumentno(String documentno) {
        this.documentno = documentno;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReceivedby() {
        return receivedby;
    }

    public void setReceivedby(String receivedby) {
        this.receivedby = receivedby;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public int[] getAdCameraId() {
        return adCameraId;
    }

    public void setAdCameraId(int[] adCameraId) {
        this.adCameraId = adCameraId;
    }
}
