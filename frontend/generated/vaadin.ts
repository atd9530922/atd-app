import './vaadin-featureflags.js';

import './index';

import './theme-ampere-v2.global.generated.js';
import { applyTheme } from './theme.js';
applyTheme(document);
